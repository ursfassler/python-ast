from typing import Callable

from nodes import Base, Module, Function, ResolvedReference, Reference
from remover import ImportRemover, LambdaRemover, RemoveBelowLeaf


def encapsulate_module_init(node: Base) -> None:
    if isinstance(node, Module):
        if node.reference:
            init = Function("--init--")
            init.reference = node.reference
            node.reference = []
            node.children.append(init)

    for child in node.children:
        encapsulate_module_init(child)


def filter_references(node: Base, keep_predicate: Callable[[Reference], bool]) -> None:
    node.reference = [x for x in node.reference if keep_predicate(x)]

    for child in node.children:
        filter_references(child, keep_predicate)


def clean_ast(root: Base) -> None:
    remover1 = ImportRemover()
    remover1.remove(root)

    remover2 = LambdaRemover()
    remover2.remove(root)

    remover3 = RemoveBelowLeaf()
    remover3.remove(root)

    filter_references(root, lambda x: isinstance(x, ResolvedReference))
    encapsulate_module_init(root)
