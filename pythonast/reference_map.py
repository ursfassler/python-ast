from typing import Set, Dict, Optional, List

from nodes import Base, ResolvedReference, Reference


class ReferenceMap:
    def __init__(self) -> None:
        self.__map: Set[Base] = set()

    def reduce(self) -> None:
        reduced: Dict[Base, List[Reference]] = {}
        for key in self.__map:
            self.__reduce_and_store(key, reduced)
        for key in self.__map:
            key.reference = reduced[key]

    def __reduce_and_store(self, key: Base, reduced: Dict[Base, List[Reference]]) -> None:
        if key in reduced:
            return
        refs: List[Reference] = []
        for ref in key.reference:
            if isinstance(ref, ResolvedReference):
                target = ref.target()
                if self.__should_reduce(target):
                    self.__reduce_and_store(target, reduced)
                    refs += reduced[target]
                else:
                    refs.append(ref)
            else:
                refs.append(ref)
        reduced[key] = refs

    def __should_reduce(self, value: Base) -> bool:
        return value in self.__map

    def add(self, child: Base) -> None:
        self.__map.add(child)

    def value(self, key: Base) -> Optional[List[Reference]]:
        return key.reference if (key in self.__map) else None
