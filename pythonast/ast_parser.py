from pathlib import Path
from typing import Dict, List

import astroid
from lxml.etree import Element

from ast_visitor import AstVisitor
from cleanup import clean_ast
from linker import Linker
from nodes import Module, Base
from printer import print_nodes
from writer import write_xml


def parse_project(files: Dict[str, str], root_path: Path) -> Element:
    root_name = root_path.name
    root = Module(root_name)

    for filename in sorted(files):
        print(f"parsing {filename}")
        content = files[filename]
        __parse(content, filename, root, root_path)

    linker = Linker()
    linker.link(root)

    print_nodes(root)

    clean_ast(root)

    xml_root = Element("project")
    xml_root.set("language", "Python")
    write_xml(root, xml_root)

    return xml_root


def __parse(content: str, filename: str, root: Module, root_path: Path) -> None:
    module_path = __module_path(filename, root_path)

    parent = __make_module_path(root, module_path[:-1])
    fa = astroid.parse(content, module_path[-1], filename)
    if fa.package:
        module = parent
    else:
        module = Module(fa.name)
        parent.children.append(module)
    visitor = AstVisitor(module)
    visitor.visit(fa)


def parse_module(module: Module, content: str) -> None:
    fa = astroid.parse(content, module.name)
    visitor = AstVisitor(module)
    visitor.visit(fa)


def __make_module_path(root: Module, path: List[str]) -> Base:
    module: Base = root
    for name in path:
        child = module.find(name)
        if not child:
            child = Module(name)
            module.children.append(child)
        module = child
    return module


def __module_path(filename: str, root_path: Path) -> List[str]:
    file_path = Path(filename)
    relative = str(file_path.relative_to(root_path))

    suffix = ".py"
    if relative.endswith(suffix):
        name = relative[:-len(suffix)]
    else:
        name = relative

    path = name.split("/") if name else []
    module_name = path

    return module_name
