from abc import abstractmethod, ABCMeta
from typing import List, Optional, Any


class Reference(metaclass=ABCMeta):
    @abstractmethod
    def accept(self, visitor: 'ReferenceVisitor') -> None:
        raise NotImplementedError


class Base:
    def __init__(self, xname: str) -> None:
        self.name = xname
        self.children: List['Base'] = []
        self.reference: List[Reference] = []
        self.globals: List[str] = []

    def __repr__(self) -> str:
        return self.name + "(" + str(type(self)) + ")"

    def find(self, name: str) -> Optional['Base']:
        for child in self.children:
            if child.name == name:
                return child

        return None

    @abstractmethod
    def accept(self, visitor: 'NodeVisitor') -> None:
        raise NotImplementedError


class Module(Base):
    def accept(self, visitor: 'NodeVisitor') -> None:
        return visitor.visit_module(self)


class Class(Base):
    def accept(self, visitor: 'NodeVisitor') -> None:
        return visitor.visit_class(self)


class Method(Base):
    def accept(self, visitor: 'NodeVisitor') -> None:
        return visitor.visit_method(self)


class Field(Base):
    def accept(self, visitor: 'NodeVisitor') -> None:
        return visitor.visit_field(self)


class Function(Base):
    def accept(self, visitor: 'NodeVisitor') -> None:
        return visitor.visit_function(self)


class Variable(Base):
    def accept(self, visitor: 'NodeVisitor') -> None:
        return visitor.visit_variable(self)


class Lambda(Base):
    def __init__(self) -> None:
        Base.__init__(self, "lambda")

    def accept(self, visitor: 'NodeVisitor') -> None:
        return visitor.visit_lambda(self)


class Assign(Base):
    def __init__(self) -> None:
        Base.__init__(self, "assign")

    def accept(self, visitor: 'NodeVisitor') -> None:
        return visitor.visit_assign(self)


class Import(Base):
    def accept(self, visitor: 'NodeVisitor') -> None:
        return visitor.visit_import(self)


class NodeVisitor(metaclass=ABCMeta):
    @abstractmethod
    def before_visit(self, node: Base) -> None:
        raise NotImplementedError

    @abstractmethod
    def after_visit(self, node: Base) -> None:
        raise NotImplementedError

    @abstractmethod
    def visit_module(self, node: Module) -> None:
        raise NotImplementedError

    @abstractmethod
    def visit_class(self, node: Class) -> None:
        raise NotImplementedError

    @abstractmethod
    def visit_method(self, node: Method) -> None:
        raise NotImplementedError

    @abstractmethod
    def visit_function(self, node: Function) -> None:
        raise NotImplementedError

    @abstractmethod
    def visit_variable(self, node: Variable) -> None:
        raise NotImplementedError

    @abstractmethod
    def visit_field(self, node: Field) -> None:
        raise NotImplementedError

    @abstractmethod
    def visit_import(self, node: Import) -> None:
        raise NotImplementedError

    @abstractmethod
    def visit_lambda(self, node: Lambda) -> None:
        raise NotImplementedError

    @abstractmethod
    def visit_assign(self, node: Assign) -> None:
        raise NotImplementedError


class NodeNullVisitor(NodeVisitor):
    def before_visit(self, node: Base) -> None:
        pass

    def after_visit(self, node: Base) -> None:
        pass

    def default_visit(self, node: Base) -> None:
        pass

    def visit_module(self, node: Module) -> None:
        return self.default_visit(node)

    def visit_class(self, node: Class) -> None:
        return self.default_visit(node)

    def visit_method(self, node: Method) -> None:
        return self.default_visit(node)

    def visit_function(self, node: Function) -> None:
        return self.default_visit(node)

    def visit_variable(self, node: Variable) -> None:
        return self.default_visit(node)

    def visit_field(self, node: Field) -> None:
        return self.default_visit(node)

    def visit_import(self, node: Import) -> None:
        return self.default_visit(node)

    def visit_lambda(self, node: Lambda) -> None:
        return self.default_visit(node)

    def visit_assign(self, node: Assign) -> None:
        pass


class UnresolvedReferencePath(Reference):
    def __init__(self, target: List[str]) -> None:
        self.__target = target

    def __repr__(self) -> str:
        return "\"" + ".".join(self.__target) + "\""

    def target(self) -> List[str]:
        return self.__target

    def accept(self, visitor: 'ReferenceVisitor') -> None:
        return visitor.visit_unresolved_reference_path(self)


class ResolvedReference(Reference):
    def __init__(self, target: Base) -> None:
        self.__target = target

    def __repr__(self) -> str:
        return str(self.__target)

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, ResolvedReference):
            return self.__target.__eq__(other.__target)
        else:
            return False

    def target(self) -> Base:
        return self.__target

    def accept(self, visitor: 'ReferenceVisitor') -> None:
        return visitor.visit_resolved_reference(self)


class ReferenceVisitor(metaclass=ABCMeta):
    @abstractmethod
    def visit_unresolved_reference_path(self, reference: UnresolvedReferencePath) -> None:
        raise NotImplementedError

    @abstractmethod
    def visit_resolved_reference(self, reference: ResolvedReference) -> None:
        raise NotImplementedError


class ReferenceNullVisitor(ReferenceVisitor):
    def visit_unresolved_reference_path(self, reference: UnresolvedReferencePath) -> None:
        self.default_visit(reference)

    def visit_resolved_reference(self, reference: ResolvedReference) -> None:
        self.default_visit(reference)

    def default_visit(self, reference: Reference) -> None:
        pass


class Traverser:
    def __init__(self, node_visitor: Optional[NodeVisitor] = None,
                 reference_visitor: Optional[ReferenceVisitor] = None) -> None:
        self.__node_visitor = node_visitor
        self.__reference_visitor = reference_visitor

    def traverse(self, node: Base) -> None:
        if self.__node_visitor:
            self.__node_visitor.before_visit(node)

        if self.__node_visitor:
            node.accept(self.__node_visitor)

        if self.__reference_visitor:
            for reference in node.reference:
                reference.accept(self.__reference_visitor)

        for child in node.children:
            self.traverse(child)

        if self.__node_visitor:
            self.__node_visitor.after_visit(node)


def traverse(node: Base, node_visitor: Optional[NodeVisitor] = None,
             reference_visitor: Optional[ReferenceVisitor] = None) -> None:
    traverser = Traverser(node_visitor=node_visitor, reference_visitor=reference_visitor)
    traverser.traverse(node)
