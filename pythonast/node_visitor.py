from astroid.node_classes import NodeNG


class NodeVisitor:
    def generic_visit(self, node: NodeNG) -> None:
        if hasattr(node, "_astroid_fields"):
            for name in node._astroid_fields:
                value = getattr(node, name)
                self._visit_generic(value)

    def _visit_generic(self, node: NodeNG) -> None:
        if isinstance(node, list):
            for child in node:
                self._visit_generic(child)
        elif isinstance(node, tuple):
            for child in node:
                self._visit_generic(child)
        elif not node or isinstance(node, str):
            pass
        else:
            self.visit(node)

    def visit(self, node: NodeNG) -> None:
        attr = "visit_" + node.__class__.__name__.lower()
        if hasattr(self, attr):
            func = getattr(self, attr)
            func(node)
        else:
            self.generic_visit(node)
