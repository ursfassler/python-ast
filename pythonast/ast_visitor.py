import logging
from copy import copy
from typing import Optional, List

import astroid
from astroid.node_classes import NodeNG

from node_visitor import NodeVisitor
from nodes import (
    Base,
    UnresolvedReferencePath,
    Class,
    Function,
    Method,
    Variable,
    ResolvedReference,
    Assign,
    Module,
    Field,
    Import,
    Lambda,
)


class AstVisitor(NodeVisitor):
    def __init__(self, module: Base) -> None:
        self.__stack = [module]

    def visit_importfrom(self, node: astroid.ImportFrom) -> None:
        context = self.__get_context()

        module = node.modname.split(".")
        names = node.names

        for alias in names:
            target_name = alias[0]
            local_name = alias[1] if alias[1] else alias[0]

            if target_name == "*":
                logging.error("import * is not supported")
            else:
                target_path = module + [target_name]
                child = Import(local_name)
                child.reference.append(UnresolvedReferencePath(target_path))
                context.children.append(child)
        self.__before(node)
        self.__after(node)

    def visit_assignname(self, node: astroid.AssignName) -> None:
        child: Optional[Base] = None

        context = self.__get_context()
        if node.name in context.globals:
            context.reference.append(UnresolvedReferencePath([node.name]))
        else:
            var = context.find(node.name)
            if not var:
                if isinstance(context, Class):
                    child = Field(node.name)
                else:
                    child = Variable(node.name)
                self.__push_child(child)

        self.__before(node)
        self.__after(node)

        if child is not None:
            self.__pop_child()

    def visit_name(self, node: astroid.Name) -> None:
        # inf = node.inferred()
        self.__current().reference.append(UnresolvedReferencePath([node.name]))

        self.__before(node)
        self.__after(node)

    def visit_global(self, node: astroid.Global) -> None:
        self.__current().globals += node.names
        self.__before(node)
        self.__after(node)

    def visit_assign(self, node: astroid.Assign) -> None:
        assign = Assign()
        self.__push_child(assign)
        self.__before(node)
        self.__after(node)
        self.__pop_child()

        self.__current().children.remove(assign)  # TODO make sure we have no references to it

        context = self.__get_context()
        if isinstance(context, Module):
            # FIXME: for variable assignments in modules
            for target in assign.children:
                self.__current().children.append(target)
                for reference in assign.reference:
                    target.reference.append(copy(reference))
        else:
            # FIXME: for function calls in functions
            for target in assign.children:
                self.__current().children.append(target)
            for reference in assign.reference:
                self.__current().reference.append(copy(reference))

    def visit_classdef(self, node: astroid.ClassDef) -> None:
        child = Class(node.name)
        self.__push_child(child)
        self.__before(node)

        self.__after(node)
        self.__pop_child()

    def visit_assignattr(self, node: astroid.AssignAttr) -> None:
        if isinstance(node.expr, astroid.Name) and (node.expr.name == "self"):
            class_def = self.__get_class_definition()
            field = class_def.find(node.attrname)
            if field is None:
                field = Field(node.attrname)
                class_def.children.append(field)

            context = self.__get_context()
            context.reference.append(ResolvedReference(field))
        else:
            self.__before(node)
            self.__after(node)

    def visit_attribute(self, node: astroid.Attribute) -> None:
        path = self.__find_simple_path(node)
        if path:
            self.__current().reference.append(UnresolvedReferencePath(path))
        elif isinstance(node.expr, astroid.Name):
            self.__current().reference.append(UnresolvedReferencePath([node.expr.name, node.attrname]))

    def __find_simple_path(self, node: NodeNG) -> Optional[List[str]]:
        path = []
        while isinstance(node, astroid.Attribute):
            path.append(node.attrname)
            node = node.expr

        if isinstance(node, astroid.Name):
            path.append(node.name)
            return list(reversed(path))
        else:
            return None

    def visit_functiondef(self, node: astroid.FunctionDef) -> None:
        child = Method(node.name) if isinstance(self.__current(), Class) else Function(node.name)
        self.__push_child(child)
        self.__before(node)
        self.__after(node)
        self.__pop_child()

    def visit_lambda(self, node: astroid.Lambda) -> None:
        child = Lambda()
        self.__push_child(child)
        self.__before(node)
        self.__after(node)
        self.__pop_child()

    def visit_arguments(self, node: astroid.Arguments) -> None:
        self.__before(node)
        self.__after(node)

    def generic_visit(self, node: NodeNG) -> None:
        self.__before(node)
        self.__after(node)

    def __get_class_definition(self) -> Class:
        for base in reversed(self.__stack):
            if isinstance(base, Class):
                return base
        raise TypeError("no class found")

    def __current(self) -> Base:
        return self.__stack[-1]

    def __push_child(self, child: Base) -> None:
        self.__current().children.append(child)
        self.__stack.append(child)

    def __pop_child(self) -> None:
        self.__stack.pop()

    def __before(self, node: NodeNG) -> None:
        pass

    def __after(self, node: NodeNG) -> None:
        NodeVisitor.generic_visit(self, node)

    def __get_context(self) -> Base:
        for base in reversed(self.__stack):
            if isinstance(base, (Lambda, Function, Method, Class, Module)):
                return base
        raise TypeError("no context found")
