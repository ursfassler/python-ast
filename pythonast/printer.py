from typing import Dict

from nodes import (
    Base,
    Reference,
    ReferenceVisitor,
    UnresolvedReferencePath,
    ResolvedReference,
    NodeNullVisitor,
    traverse
)


class ShortId(NodeNullVisitor):
    def __init__(self) -> None:
        self.__next = 1
        self.__ids: Dict[Base, int] = {}

    def get_ids(self) -> Dict[Base, int]:
        return self.__ids

    def default_visit(self, node: Base) -> None:
        self.__ids[node] = self.__next
        self.__next += 1


class Names:
    def __init__(self, ids: Dict[Base, int]) -> None:
        self.__ids = ids

    def name(self, node: Base) -> str:
        id = self.__ids[node] if (node in self.__ids) else "<undef>"
        return f"{node.name} ({id})"


class ReferenceName(ReferenceVisitor):
    def __init__(self, names: Names) -> None:
        self.__names = names
        self.name = ""

    def visit_unresolved_reference_path(self, reference: UnresolvedReferencePath) -> None:
        self.name = reference.__repr__()

    def visit_resolved_reference(self, reference: ResolvedReference) -> None:
        self.name = self.__names.name(reference.target())


class Printer(NodeNullVisitor):
    def __init__(self, ids: Dict[Base, int]) -> None:
        self.__names = Names(ids)
        self.__reference_name = ReferenceName(self.__names)
        self.__depth = 0

    def default_visit(self, node: Base) -> None:
        indent = "  " * (self.__depth - 1)
        ref_names = [self.__ref_name(x) for x in node.reference]
        refs = ", ".join(ref_names)
        print(indent + f"{self.__names.name(node)}: -> {refs}")

    def before_visit(self, node: Base) -> None:
        self.__depth += 1

    def after_visit(self, node: Base) -> None:
        self.__depth -= 1

    def __ref_name(self, ref: Reference) -> str:
        ref.accept(self.__reference_name)
        return self.__reference_name.name


def print_nodes(module: Base) -> None:
    short_id = ShortId()
    traverse(module, short_id)
    ids = short_id.get_ids()

    printer = Printer(ids)
    traverse(module, printer)
