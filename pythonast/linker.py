from typing import Optional, List

from nodes import Base, UnresolvedReferencePath, ResolvedReference, Class


class Linker:
    def __init__(self) -> None:
        self.__stack: List[Base] = []

    def link(self, top: Base) -> None:
        self.__stack.append(top)

        new_ref = []
        for ref in top.reference:
            if isinstance(ref, UnresolvedReferencePath):
                target = self.__find_ref(ref.target())
                if target is not None:
                    ref = ResolvedReference(target)
                new_ref.append(ref)
            else:
                new_ref.append(ref)
        top.reference = new_ref

        for child in top.children:
            self.link(child)

        self.__stack.pop()

    def __find_ref(self, target: List[str]) -> Optional[Base]:
        assert len(target) >= 1
        name = target[0]

        if name == "self":
            for obj in reversed(self.__stack):
                if isinstance(obj, Class):
                    leaf = self.__find_leaf(obj, target[1:])
                    return leaf

        for obj in reversed(self.__stack):
            res = obj.find(name)
            leaf = self.__find_leaf(res, target[1:])
            if leaf:
                return leaf

        root = self.__stack[0]
        if name == root.name:
            res = root
            leaf = self.__find_leaf(res, target[1:])
            if leaf:
                return leaf

        return None

    def __find_leaf(self, res: Optional[Base], target: List[str]) -> Optional[Base]:
        for part in target:
            if res is None:
                return None
            res = res.find(part)
        return res
