import unittest
from typing import List, Optional

from nodes import ResolvedReference, Class, Reference
from reference_map import ReferenceMap


def list(optlist: Optional[List[Reference]]) -> List[Reference]:
    assert optlist
    return optlist


class ReferenceMapTest(unittest.TestCase):
    def setUp(self) -> None:
        self.__testee = ReferenceMap()

    def test_reduce_does_nothing_when_target_is_not_in_map(self) -> None:
        node1 = Class("1")
        node2 = Class("2")
        node1.reference.append(ResolvedReference(node2))
        self.__testee.add(node1)

        self.__testee.reduce()

        self.assertSequenceEqual([ResolvedReference(node2)], list(self.__testee.value(node1)))

    def test_reduce_replaces_referenced_when_target_is_in_map(self) -> None:
        node1 = Class("1")
        node2 = Class("2")
        node3 = Class("3")
        node1.reference.append(ResolvedReference(node2))
        node2.reference.append(ResolvedReference(node3))
        self.__testee.add(node1)
        self.__testee.add(node2)

        self.__testee.reduce()

        self.assertSequenceEqual([ResolvedReference(node3)], list(self.__testee.value(node1)))

    def test_reduce_replaces_2_references_when_targets_are_in_map(self) -> None:
        node3 = Class("3")
        node1 = Class("1")
        node2 = Class("2")
        node4 = Class("4")
        node1.reference.append(ResolvedReference(node2))
        node2.reference.append(ResolvedReference(node3))
        node3.reference.append(ResolvedReference(node4))
        self.__testee.add(node1)
        self.__testee.add(node2)
        self.__testee.add(node3)

        self.__testee.reduce()

        self.assertSequenceEqual([ResolvedReference(node4)], list(self.__testee.value(node1)))
