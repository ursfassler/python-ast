from copy import copy
from typing import List

from nodes import Base, ResolvedReference, Import, Lambda, Method, Function, Reference
from reference_map import ReferenceMap


class ImportRemover:
    def __init__(self) -> None:
        self.__map = ReferenceMap()

    def remove(self, node: Base) -> None:
        self.__remove(node)
        self.__map.reduce()
        self.__relink(node)

    def __remove(self, node: Base) -> None:
        new_children = []
        for child in node.children:
            if isinstance(child, Import):
                self.__map.add(child)
            else:
                self.__remove(child)
                new_children.append(child)

        node.children = new_children

    def __relink(self, node: Base) -> None:
        new_references = []
        for reference in node.reference:
            if isinstance(reference, ResolvedReference):
                references = self.__map.value(reference.target())
                if references:
                    for new_ref in references:
                        new_references.append(copy(new_ref))
                else:
                    new_references.append(reference)
            else:
                new_references.append(reference)
        node.reference = new_references

        for child in node.children:
            self.__relink(child)


class LambdaRemover:
    def remove(self, node: Base) -> None:
        new_children = []
        for child in node.children:
            if isinstance(child, Lambda):
                for reference in child.reference:
                    node.reference.append(reference)
                for grandchild in child.children:
                    node.children.append(grandchild)
            else:
                self.remove(child)
                new_children.append(child)

        node.children = new_children


class ChildrenRemover:
    def __init__(self) -> None:
        self.__children: List[Base] = []
        self.__reference: List[Reference] = []

    def remove_children(self, node: Base) -> None:
        self.__init()
        self.__remove(node)
        self.__update(node)

    def __init(self) -> None:
        self.__children = []
        self.__reference = []

    def __update(self, node: Base) -> None:
        node.reference += self.__reference

        new_references: List[Reference] = []
        for reference in node.reference:
            if isinstance(reference, ResolvedReference):
                target = reference.target()
                if target not in self.__children:
                    new_references.append(reference)
            else:
                new_references.append(reference)
        node.reference = new_references

    def __remove(self, node: Base) -> None:
        self.__children += node.children

        for child in node.children:
            for reference in child.reference:
                self.__reference.append(reference)
            self.__remove(child)

        node.children = []


class RemoveBelowLeaf:
    def __init__(self) -> None:
        self.__remover = ChildrenRemover()

    def remove(self, node: Base) -> None:
        if isinstance(node, (Method, Function)):
            self.__remover.remove_children(node)

        for child in node.children:
            self.remove(child)
