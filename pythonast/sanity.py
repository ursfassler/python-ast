from typing import Set

from nodes import NodeNullVisitor, Base, ReferenceNullVisitor, ResolvedReference, traverse, Module


class TreeNodes(NodeNullVisitor):
    def __init__(self) -> None:
        self.__nodes: Set[Base] = set()

    def default_visit(self, node: Base) -> None:
        assert node not in self.__nodes
        self.__nodes.add(node)

    def get_nodes(self) -> Set[Base]:
        return self.__nodes


class ReferencedNodes(ReferenceNullVisitor):
    def __init__(self) -> None:
        self.__nodes: Set[Base] = set()

    def visit_resolved_reference(self, reference: ResolvedReference) -> None:
        self.__nodes.add(reference.target())

    def get_nodes(self) -> Set[Base]:
        return self.__nodes


def verify_that_all_references_are_defined(module: Module) -> None:
    tree = TreeNodes()
    traverse(module, tree)
    refs = ReferencedNodes()
    traverse(module, reference_visitor=refs)
    lost_nodes = refs.get_nodes() - tree.get_nodes()
    if lost_nodes:
        raise RuntimeError(f"referenced but not defined nodes: {lost_nodes}")
