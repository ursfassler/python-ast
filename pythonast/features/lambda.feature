Feature: parse lambdas


Scenario: parse a simple lambda
  Given I have the source file "test.py":
  """
  def foo(x):
      pass

  def test():
    foo(lambda y: y)
  """

  When I parse the source files

  Then I expect the output:
  """
  <project language="Python">
    <package name="">
      <package name="test">
        <function name="foo()" id="1"/>
        <function name="test()">
          <reference target="1"/>
        </function>
      </package>
    </package>
  </project>

  """


Scenario: parse reference to function in lambda
  Given I have the source file "test.py":
  """
  def foo(x):
      x(42)

  def bar(y):
      print(y)

  def test():
      foo(lambda z: bar(z))
  """

  When I parse the source files

  Then I expect the output:
  """
  <project language="Python">
    <package name="">
      <package name="test">
        <function name="foo()" id="1"/>
        <function name="bar()" id="2"/>
        <function name="test()">
          <reference target="1"/>
          <reference target="2"/>
        </function>
      </package>
    </package>
  </project>

  """
