Feature: parse classes


Scenario: parse the simplest class
  Given I have the source file:
  """
  class A:
      pass
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <class name="A"/>
  </package>

  """


Scenario: parse a class with a method
  Given I have the source file:
  """
  class A:
      def foo(self):
          pass
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <class name="A">
      <method name="foo()"/>
    </class>
  </package>

  """


Scenario: parse a class with a field
  Given I have the source file:
  """
  class A:
      def __init__(self):
          self.__a = 42
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <class name="A">
      <method name="__init__()">
        <reference target="1"/>
      </method>
      <field name="__a" id="1"/>
    </class>
  </package>

  """


Scenario: parse a class with a field written form multiple methods
  Given I have the source file:
  """
  class A:
      def __init__(self):
          self.__a = 42

      def foo(self):
          self.__a = 23

  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <class name="A">
      <method name="__init__()">
        <reference target="1"/>
      </method>
      <field name="__a" id="1"/>
      <method name="foo()">
        <reference target="1"/>
      </method>
    </class>
  </package>

  """


Scenario: parameter shadows field
  Given I have the source file:
  """
  class A:
      def __init__(self):
          self.a = 42

      def foo(self, a):
          print(a)

  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <class name="A">
      <method name="__init__()">
        <reference target="1"/>
      </method>
      <field name="a" id="1"/>
      <method name="foo()"/>
    </class>
  </package>

  """


Scenario: remove function declaration in method
  Given I have the source file:
  """
  class A:
      def x(self):
          pass

      def y(self):
          def z():
              self.x()
          z()

  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <class name="A">
      <method name="x()" id="1"/>
      <method name="y()">
        <reference target="1"/>
      </method>
    </class>
  </package>

  """


Scenario: remove nested functions declaration in method
  Given I have the source file:
  """
  class A:
      def x(self):
          def y():
              def z():
                  self.a = 42
              zx()
          y()

  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <class name="A">
      <method name="x()">
        <reference target="1"/>
      </method>
      <field name="a" id="1"/>
    </class>
  </package>

  """


Scenario: parse a class with a static field
  Given I have the source file:
  """
  class A:
      __a = 42
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <class name="A">
      <field name="__a"/>
    </class>
  </package>

  """
