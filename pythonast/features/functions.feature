Feature: parse functions


Scenario: parse empty file
  Given I have the source file:
  """
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name=""/>

  """


Scenario: parse simplest function
  Given I have the source file:
  """
  def foo():
      pass
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <function name="foo()"/>
  </package>

  """


Scenario: parse dependency from function to function
  Given I have the source file:
  """
  def foo():
      pass

  def bar():
      foo()
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <function name="foo()" id="1"/>
    <function name="bar()">
      <reference target="1"/>
    </function>
  </package>

  """


Scenario: parse dependency to function in expression
  Given I have the source file:
  """
  def foo():
      return 42

  def bar():
      a = foo()
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <function name="foo()" id="1"/>
    <function name="bar()">
      <reference target="1"/>
    </function>
  </package>

  """


Scenario: don't add function local variables
  Given I have the source file:
  """
  def func():
      a = 42
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <function name="func()"/>
  </package>

  """


Scenario: parse dependency from function to module variable when reading
  Given I have the source file:
  """
  a = 1123

  def func():
      return a
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <variable name="a" id="1"/>
    <function name="func()">
      <reference target="1"/>
    </function>
  </package>

  """


Scenario: parse dependency from function to module variable when writing
  Given I have the source file:
  """
  a = 1

  def func():
      global a
      a = 3
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <variable name="a" id="1"/>
    <function name="func()">
      <reference target="1"/>
    </function>
  </package>

  """


Scenario: don't add dependency from function to module variable with the same name when writing
  Given I have the source file:
  """
  a = 1

  def func():
      a = 2
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <variable name="a"/>
    <function name="func()"/>
  </package>

  """


Scenario: parameter shadows variable
  Given I have the source file:
  """
  a = 42

  def foo(self, a):
      print(a)

  """

    When I parse the source file

    Then I expect the output:
    """
    <package name="">
      <variable name="a"/>
      <function name="foo()"/>
    </package>

    """


Scenario: write reference returned by function
  Given I have the source file:
  """
  class A:
      def __init__(self):
          self.field = 1

  a = A()

  def foo():
      return a

  def bar():
      foo().field = 2
  """

    When I parse the source file

    Then I expect the output:
    """
    <package name="">
      <class name="A" id="1">
        <method name="__init__()">
          <reference target="2"/>
        </method>
        <field name="field" id="2"/>
      </class>
      <variable name="a" id="3">
        <reference target="1"/>
      </variable>
      <function name="foo()" id="4">
        <reference target="3"/>
      </function>
      <function name="bar()">
        <reference target="4"/>
      </function>
    </package>

    """


Scenario: remove nested functions declaration in functions
  Given I have the source file:
  """
  a = 0
  def x():
      def y():
          def z():
              global a
              a = 42
          zx()
      y()

  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <variable name="a" id="1"/>
    <function name="x()">
      <reference target="1"/>
    </function>
  </package>

  """
