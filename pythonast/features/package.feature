Feature: parse package


Scenario: link variable from a different package
  Given I have the source file "test/pac_a/mod_a.py":
  """
  var_a = ""
  """
  And I have the source file "test/pac_b/mod_b.py":
  """
  from test.pac_a.mod_a import var_a
  var_b = var_a
  """

  When I parse the source files in "test"

  Then I expect the output:
  """
  <project language="Python">
    <package name="test">
      <package name="pac_a">
        <package name="mod_a">
          <variable name="var_a" id="1"/>
        </package>
      </package>
      <package name="pac_b">
        <package name="mod_b">
          <variable name="var_b">
            <reference target="1"/>
          </variable>
        </package>
      </package>
    </package>
  </project>

  """


Scenario: link variable from a relative sub package
  Given I have the source file "root/pac_a/pac_b/pac_c/mod_c.py":
  """
  var_c = ""
  """
  And I have the source file "root/pac_a/pac_b/mod_b.py":
  """
  from .pac_c.mod_c import var_c
  var_b = var_c
  """

  When I parse the source files in "root"

  Then I expect the output:
  """
  <project language="Python">
    <package name="root">
      <package name="pac_a">
        <package name="pac_b">
          <package name="mod_b">
            <variable name="var_b">
              <reference target="1"/>
            </variable>
          </package>
          <package name="pac_c">
            <package name="mod_c">
              <variable name="var_c" id="1"/>
            </package>
          </package>
        </package>
      </package>
    </package>
  </project>

  """


Scenario: parse project within file system
  Given I have the source file "/home/user/stuff/test/run.py":
  """
  """

  When I parse the source files in "/home/user/stuff/test/"

  Then I expect the output:
  """
  <project language="Python">
    <package name="test">
      <package name="run"/>
    </package>
  </project>

  """


Scenario: link variable from module with same name as package
  Given I have the source file "test/test.py":
  """
  var_a = ""
  """
  And I have the source file "test/main.py":
  """
  from test.test import var_a
  var_b = var_a
  """

  When I parse the source files in "test"

  Then I expect the output:
  """
  <project language="Python">
    <package name="test">
      <package name="main">
        <variable name="var_b">
          <reference target="1"/>
        </variable>
      </package>
      <package name="test">
        <variable name="var_a" id="1"/>
      </package>
    </package>
  </project>

  """


Scenario: link variable from module with same name as package while referenced with whole name
  Given I have the source file "test/test.py":
  """
  var_a = ""
  """
  And I have the source file "test/main.py":
  """
  import test.test
  var_b = test.test.var_a
  """

  When I parse the source files in "test"

  Then I expect the output:
  """
  <project language="Python">
    <package name="test">
      <package name="main">
        <variable name="var_b">
          <reference target="1"/>
        </variable>
      </package>
      <package name="test">
        <variable name="var_a" id="1"/>
      </package>
    </package>
  </project>

  """


Scenario: link variable from __init__.py
  Given I have the source file "test/foo/__init__.py":
  """
  var_a = ""
  """
  And I have the source file "test/bar.py":
  """
  from test.foo import var_a
  var_b = var_a
  """

  When I parse the source files in "test"

  Then I expect the output:
  """
  <project language="Python">
    <package name="test">
      <package name="bar">
        <variable name="var_b">
          <reference target="1"/>
        </variable>
      </package>
      <package name="foo">
        <variable name="var_a" id="1"/>
      </package>
    </package>
  </project>

  """


Scenario: link class access by transitive import
  Given I have the source file "test/foo/__init__.py":
  """
  from test.foo.mod_a import A
  """
  And I have the source file "test/foo/mod_a/__init__.py":
  """
  class A:
      pass
  """
  And I have the source file "test/__main__.py":
  """
  from test.foo import A
  a = A()
  """

  When I parse the source files in "test"

  Then I expect the output:
  """
  <project language="Python">
    <package name="test">
      <package name="__main__">
        <variable name="a">
          <reference target="1"/>
        </variable>
      </package>
      <package name="foo">
        <package name="mod_a">
          <class name="A" id="1"/>
        </package>
      </package>
    </package>
  </project>

  """


Scenario: link class access by 2 times transitive import
  Given I have the source file "test/foo/bar/mod_a.py":
  """
  class A:
      pass
  """
  And I have the source file "test/foo/bar/__init__.py":
  """
  from .mod_a import A
  """
  And I have the source file "test/foo/__init__.py":
  """
  from .bar import A
  """
  And I have the source file "test/__main__.py":
  """
  from test.foo import A
  a = A()
  """

  When I parse the source files in "test"

  Then I expect the output:
  """
  <project language="Python">
    <package name="test">
      <package name="__main__">
        <variable name="a">
          <reference target="1"/>
        </variable>
      </package>
      <package name="foo">
        <package name="bar">
          <package name="mod_a">
            <class name="A" id="1"/>
          </package>
        </package>
      </package>
    </package>
  </project>

  """


Scenario: link to package
  Given I have the source file "test/foo/__init__.py":
  """
  """
  And I have the source file "test/bar.py":
  """
  from test import foo
  def test():
      print(foo)
  """

  When I parse the source files in "test"

  Then I expect the output:
  """
  <project language="Python">
    <package name="test">
      <package name="bar">
        <function name="test()">
          <reference target="1"/>
        </function>
      </package>
      <package name="foo" id="1"/>
    </package>
  </project>

  """
