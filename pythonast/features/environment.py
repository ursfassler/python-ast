import logging
from typing import Any

from features.steps.LoggingHander import Handler


def before_scenario(context: Any, scenario: Any) -> None:
    context.logger = logging.getLogger()
    context.handler = Handler()
    context.logger.addHandler(context.handler)
    context.sources = {}


def after_scenario(context: Any, scenario: Any) -> None:
    context.logger.removeHandler(context.handler)
