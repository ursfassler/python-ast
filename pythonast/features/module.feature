Feature: parse modules


Scenario: parse 2 modules
  Given I have the source file "X.py":
  """
  a = 42
  """
  And I have the source file "Y.py":
  """
  a = 42
  """

  When I parse the source files

  Then I expect the output:
  """
  <project language="Python">
    <package name="">
      <package name="X">
        <variable name="a"/>
      </package>
      <package name="Y">
        <variable name="a"/>
      </package>
    </package>
  </project>

  """


Scenario: parse module in packages
  Given I have the source file "X/Y/Z.py":
  """
  a = 42
  """

  When I parse the source files

  Then I expect the output:
  """
  <project language="Python">
    <package name="">
      <package name="X">
        <package name="Y">
          <package name="Z">
            <variable name="a"/>
          </package>
        </package>
      </package>
    </package>
  </project>

  """


Scenario: parse modules in overlapping packages
  Given I have the source file "X/Y.py":
  """
  a = 42
  """
  And I have the source file "X/Z.py":
  """
  a = 42
  """

  When I parse the source files

  Then I expect the output:
  """
  <project language="Python">
    <package name="">
      <package name="X">
        <package name="Y">
          <variable name="a"/>
        </package>
        <package name="Z">
          <variable name="a"/>
        </package>
      </package>
    </package>
  </project>

  """


Scenario: link variable from other module with from xxx import xxx
  Given I have the source file "X.py":
  """
  a = 42
  """
  And I have the source file "Y.py":
  """
  from X import a
  b = a
  """

  When I parse the source files

  Then I expect the output:
  """
  <project language="Python">
    <package name="">
      <package name="X">
        <variable name="a" id="1"/>
      </package>
      <package name="Y">
        <variable name="b">
          <reference target="1"/>
        </variable>
      </package>
    </package>
  </project>

  """


Scenario: link variable from other module with from xxx import xxx as xxx
  Given I have the source file "x.py":
  """
  a = 42
  """
  And I have the source file "y.py":
  """
  from x import a as b
  c = b
  """

  When I parse the source files

  Then I expect the output:
  """
  <project language="Python">
    <package name="">
      <package name="x">
        <variable name="a" id="1"/>
      </package>
      <package name="y">
        <variable name="c">
          <reference target="1"/>
        </variable>
      </package>
    </package>
  </project>

  """


Scenario: link variable from other module with from import xxx
  Given I have the source file "X.py":
  """
  a = 42
  """
  And I have the source file "Y.py":
  """
  import X
  b = X.a
  """

  When I parse the source files

  Then I expect the output:
  """
  <project language="Python">
    <package name="">
      <package name="X">
        <variable name="a" id="1"/>
      </package>
      <package name="Y">
        <variable name="b">
          <reference target="1"/>
        </variable>
      </package>
    </package>
  </project>

  """


Scenario: don't support import of everything
  Given I have the source file "X.py":
  """
  a = 42
  """
  And I have the source file "Y.py":
  """
  from X import *
  b = a
  """

  When I parse the source files

  Then I expect the error "import * is not supported"
  And I expect the output:
  """
  <project language="Python">
    <package name="">
      <package name="X">
        <variable name="a"/>
      </package>
      <package name="Y">
        <variable name="b"/>
      </package>
    </package>
  </project>

  """


Scenario: encapsulate init code
  Given I have the source file "x.py":
  """
  a = 42

  print(a)
  """

  When I parse the source files

  Then I expect the output:
  """
  <project language="Python">
    <package name="">
      <package name="x">
        <variable name="a" id="1"/>
        <function name="--init--()">
          <reference target="1"/>
        </function>
      </package>
    </package>
  </project>

  """


Scenario: don't add init code when we have nothing to reference
  Given I have the source file "x.py":
  """
  print(__name__)
  """

  When I parse the source files

  Then I expect the output:
  """
  <project language="Python">
    <package name="">
      <package name="x"/>
    </package>
  </project>

  """


Scenario: don't link to package when named the same as a local variable
  Given I have the source file "x.py":
  """
  """
  And I have the source file "y.py":
  """
  def foo(z):
      if z:
          x = 1
      else:
          x = 2
      print(x)
  """

  When I parse the source files

  Then I expect the output:
  """
  <project language="Python">
    <package name="">
      <package name="x"/>
      <package name="y">
        <function name="foo()"/>
      </package>
    </package>
  </project>

  """


Scenario: don't link to package when named the same as variable in list comprehension
  Given I have the source file "x.py":
  """
  """
  And I have the source file "y.py":
  """
  def test(a):
      b = [x for x in a]
  """

  When I parse the source files

  Then I expect the output:
  """
  <project language="Python">
    <package name="">
      <package name="x"/>
      <package name="y">
        <function name="test()"/>
      </package>
    </package>
  </project>

  """
