# type: ignore[no-redef]
from pathlib import Path

from behave import given, when, then
from hamcrest import assert_that, equal_to, is_in
from lxml.etree import tostring, Element

from cleanup import clean_ast
from linker import Linker
from nodes import Module
from ast_parser import parse_project, parse_module
from printer import print_nodes
from writer import write_xml


@given(u'I have the source file')
def step_impl(context):
    context.source_content = context.text


@given(u'I have the source file "{filename}"')
def step_impl(context, filename):
    context.sources[filename] = context.text


@when(u'I parse the source file')
def step_impl(context):
    module = Module("")
    parse_module(module, context.source_content)
    linker = Linker()
    linker.link(module)
    print_nodes(module)
    clean_ast(module)
    root = Element("project")
    write_xml(module, root)
    xml = list(root)[0]
    text = tostring(xml, encoding="unicode", pretty_print=True)
    context.output = text


@when(u'I parse the source files')
def step_impl(context):
    xml = parse_project(context.sources, Path(""))
    text = tostring(xml, encoding="unicode", pretty_print=True)
    context.output = text


@when(u'I parse the source files in "{directory}"')
def step_impl(context, directory):
    xml = parse_project(context.sources, Path(directory))
    text = tostring(xml, encoding="unicode", pretty_print=True)
    context.output = text


@then(u'I expect the output')
def step_impl(context):
    assert_that(context.output, equal_to(context.text))


@then(u'I expect the error "import * is not supported"')
def step_impl(context):
    assert_that("import * is not supported", is_in(context.handler.error))
