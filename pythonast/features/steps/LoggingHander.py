import logging
from typing import List


class Handler(logging.Handler):
    def __init__(self) -> None:
        super().__init__()
        self.error: List[str] = []

    def emit(self, record: logging.LogRecord) -> None:
        if record.levelno == logging.ERROR:
            self.error.append(record.msg)
