Feature: parse variables


Scenario: parse a single variable
  Given I have the source file:
  """
  a = 42
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <variable name="a"/>
  </package>

  """


Scenario: parse dependency from variable to another variable
  Given I have the source file:
  """
  a = 42
  b = a
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <variable name="a" id="1"/>
    <variable name="b">
      <reference target="1"/>
    </variable>
  </package>

  """


Scenario: write variable twice
  Given I have the source file:
  """
  a = 42
  a = 5
  """

  When I parse the source file

  Then I expect the output:
  """
  <package name="">
    <variable name="a"/>
  </package>

  """
