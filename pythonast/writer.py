from typing import Set, Dict

from lxml.etree import Element

from nodes import (
    Base,
    ResolvedReference,
    Module,
    Function,
    Class,
    Method,
    Variable,
    Field,
    NodeVisitor,
    ReferenceVisitor,
    UnresolvedReferencePath,
    NodeNullVisitor,
    ReferenceNullVisitor,
    traverse, Assign, Lambda, Import
)
from sanity import verify_that_all_references_are_defined


class Referenced(ReferenceNullVisitor):
    def __init__(self) -> None:
        self.__referenced: Set[Base] = set()

    def get_referenced(self) -> Set[Base]:
        return self.__referenced

    def visit_resolved_reference(self, reference: ResolvedReference) -> None:
        self.__referenced.add(reference.target())


class ReferencedId(NodeNullVisitor):
    def __init__(self, referenced: Set[Base]) -> None:
        self.__referenced = referenced
        self.__ids: Dict[Base, int] = {}
        self.__next = 1

    def get_ids(self) -> Dict[Base, int]:
        return self.__ids

    def default_visit(self, node: Base) -> None:
        if node in self.__referenced:
            self.__ids[node] = self.__next
            self.__next += 1


class Writer(NodeVisitor, ReferenceVisitor):
    def __init__(self, referenced: Dict[Base, int], root: Element) -> None:
        self.__referenced = referenced
        self.__nodes = [root]

    def visit_module(self, node: Module) -> None:
        self.__build("package", node.name, node)

    def visit_class(self, node: Class) -> None:
        self.__build("class", node.name, node)

    def visit_method(self, node: Method) -> None:
        self.__build("method", node.name + "()", node)

    def visit_function(self, node: Function) -> None:
        self.__build("function", node.name + "()", node)

    def visit_variable(self, node: Variable) -> None:
        self.__build("variable", node.name, node)

    def visit_field(self, node: Field) -> None:
        self.__build("field", node.name, node)

    def __build(self, type: str, name: str, node: Base) -> None:
        xml = Element(type)
        xml.set("name", name)

        if node in self.__referenced:
            id = self.__referenced[node]
            xml.set("id", str(id))

        self.__peek().append(xml)
        self.__push(xml)

    def before_visit(self, node: Base) -> None:
        pass

    def after_visit(self, node: Base) -> None:
        self.__pop()

    def visit_resolved_reference(self, reference: ResolvedReference) -> None:
        target = reference.target()
        assert target in self.__referenced
        id = self.__referenced[target]
        ref_node = Element("reference")
        ref_node.set("target", str(id))
        self.__peek().append(ref_node)

    def visit_unresolved_reference_path(self, reference: UnresolvedReferencePath) -> None:
        pass

    def __push(self, node: Element) -> None:
        self.__nodes.append(node)

    def __pop(self) -> None:
        self.__nodes.pop()

    def __peek(self) -> Element:
        return self.__nodes[-1]

    def visit_import(self, node: Import) -> None:
        pass

    def visit_lambda(self, node: Lambda) -> None:
        pass

    def visit_assign(self, node: Assign) -> None:
        pass


def write_xml(module: Module, xml: Element) -> None:
    verify_that_all_references_are_defined(module)

    ref_collector = Referenced()
    traverse(module, reference_visitor=ref_collector)
    referenced = ref_collector.get_referenced()

    referenced_id = ReferencedId(referenced)
    traverse(module, node_visitor=referenced_id)
    ids = referenced_id.get_ids()

    ref_count = len(referenced)
    id_count = len(ids)
    if ref_count != id_count:
        raise RuntimeError(f"something wrong with references ({ref_count} <-> {id_count})")

    writer = Writer(ids, xml)
    traverse(module, node_visitor=writer, reference_visitor=writer)
