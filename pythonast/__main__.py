import argparse
from pathlib import Path
from typing import List

from lxml.etree import tostring

from ast_parser import parse_project


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument("project_directory")
    parser.add_argument("output_file")
    parser.add_argument('--exclude', nargs='+')
    parser.add_argument('--file', nargs='+')
    args = parser.parse_args()

    dir_in = Path(args.project_directory)
    filename_out = Path(args.output_file)
    exclude = args.exclude if args.exclude else []
    exclude = [Path(x) for x in exclude]

    print(f"input: {dir_in}")
    print(f"output: {filename_out}")

    if args.file:
        paths = [Path(x) for x in args.file]
    else:
        paths = __list_files(dir_in, exclude)

    files = {}
    for path in paths:
        print(f"read file {path}")
        with open(path) as stream:
            content = stream.read()
            filename = str(path)
            files[filename] = content

    xml = parse_project(files, dir_in)

    text = tostring(xml, encoding="unicode", pretty_print=True)
    with open(filename_out, mode="w") as stream:
        stream.write(text)


def __list_files(dir_in: Path, exclude: List[Path]) -> List[Path]:
    paths = []
    all_paths = list(dir_in.rglob("*.py"))
    for path in all_paths:
        if not __is_sub(exclude, path):
            paths.append(path)
    return paths


def __is_sub(exclude: List[Path], path: Path) -> bool:
    for ex in exclude:
        sub = ex in path.parents
        if sub:
            return True
    return False


main()
